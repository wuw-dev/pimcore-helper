<?php

declare(strict_types=1);

namespace WoellUndWoell\PimcoreHelper\OnPremiseConfigHelper\Traits;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Yaml\Yaml;
use WoellUndWoell\PimcoreHelper\OnPremiseConfigHelper\Helper\MiscellaneousHelper;

/**
 * Trait OnPremiseConfig
 *
 * @package WoellUndWoell\PimcoreHelper\OnPremiseConfigHelper\Traits
 * @deprecated Extend WoellUndWoell\PimcoreHelper\OnPremiseConfigHelper\DependencyInjection\AbstractExtension instead of using this trait
 */
trait OnPremiseConfig
{

	/** @var string */
	protected string $finaleNamespacePart = 'DependencyInjection';

	/**
	 * Merges the on-premise-config with the existing configs according to the Config object provided
	 *
	 * @param string                 $onPremiseConfigFilePath
	 * @param array                  $configArray
	 * @param ConfigurationInterface $configuration
	 *
	 * @return array
	 */
	protected function mergeConfigsWithOnPremiseConfigs(
		array $configArray,
		ConfigurationInterface $configuration
	): array {

		$bundleNameSpace = MiscellaneousHelper::getBundleName($this);
		$bundleShortName = MiscellaneousHelper::getBundleShortName($bundleNameSpace);

		$onPremiseConfigFilePath = MiscellaneousHelper::getOnPremiseConfigFilePathForBundle($bundleNameSpace);

		if (\file_exists($onPremiseConfigFilePath)) {

			$onPremiseConfig = Yaml::parse(\file_get_contents($onPremiseConfigFilePath));

			if (\is_array($onPremiseConfig) && isset($onPremiseConfig[$bundleShortName])) {
				$configArray[] = $onPremiseConfig[$bundleShortName];
			}

		}

		$processor = new Processor();

		return $processor->processConfiguration($configuration, $configArray);

	}


	/**
	 * @param array            $configs
	 * @param ContainerBuilder $container
	 * @param string           $containerParamName
	 *
	 * @return void
	 */
	protected function putConfigsIntoContainer(
		array $configs,
		ContainerBuilder $container,
		string $containerParamName
	): void {

		if (\substr($containerParamName, 0, 1) === '%' && \substr($containerParamName, -1, 1) === '%') {
			$containerParamName = \substr($containerParamName, 1, -1);
		}

		$container->setParameter($containerParamName, $configs);
	}

}
