<?php
declare(strict_types=1);

namespace WoellUndWoell\PimcoreHelper\OnPremiseConfigHelper\Helper;

class MiscellaneousHelper
{

    private const BUNDLE_NAMESPACE_REGEX_PATTERN = '/(.+)Bundle$/';

    /**
     * Returns the name of the bundle an object belongs to, assuming it is within a subnamespace of the bundle
     *
     * @param object $object Any object
     *
     * @return string
     * @throws \Exception
     */
    public static function getBundleName(object $object): string {

        $namespace = (new \ReflectionClass($object))->getNamespaceName();

        // Go through each part of the namespace
        foreach(\explode('\\', $namespace) as $namespacePart) {
            if (preg_match(self::BUNDLE_NAMESPACE_REGEX_PATTERN, $namespacePart) === 1) {
                return $namespacePart;
            }
        }

        throw new \Exception(
            \sprintf(
                'Bundle name cannot be retrieved from namespace %s.',
                $namespace
            )
        );

    }


    /**
     * Returns a bundle's name without the "bundle" part and in lowercase
     *
     * Actually it just pops off the last part of camelCase string and returns the rest stitched back together in lowercase.
     *
     * @param string $bundleNamespace
     *
     * @return string
     */
    public static function getBundleShortName(string $bundleNamespace): string {

        $parts = \preg_split(
            pattern: '/([[:upper:]][[:lower:]]+)/',
            subject: $bundleNamespace,
            flags: PREG_SPLIT_DELIM_CAPTURE|PREG_SPLIT_NO_EMPTY
        );

        \array_pop($parts);

        return \strtolower(\implode('', $parts));

    }


	/**
	 * Returns a file path string
	 *
	 * @param string $bundle
	 *
	 * @return string
	 */
    public static function getOnPremiseConfigFilePathForBundle(string $bundle): string {
        return PIMCORE_PRIVATE_VAR . '/bundles/' . $bundle . '/onPremiseConfig.yaml';
    }

}
