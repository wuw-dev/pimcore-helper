<?php

declare(strict_types=1);

namespace WoellUndWoell\PimcoreHelper\OnPremiseConfigHelper\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use WoellUndWoell\PimcoreHelper\OnPremiseConfigHelper\Helper\MiscellaneousHelper;

abstract class AbstractConfiguration implements ConfigurationInterface
{
	/*
	 * public const NODE_EXAMPLE = [
	 *   'name'        => 'importantConfigItem',
	 *   'description' => 'This config item is used to do important stuff',
	 *   'type'        => AbstractConfiguration::NODE_TYPE_something// Any of the types provided in here. Others may be used at your own risk
	 *   'isEditable'  => true/false // If set to true, it will be available in the GUI, otherwise it won't
 	 * ]
	 */
    
    public const NODE_TYPE_SCALAR = 'scalar';
    public const NODE_TYPE_BOOL   = 'boolean';
    public const AVAILABLE_NODES  = [
    	// self::NODE_EXAMPLE
    ];

    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder(MiscellaneousHelper::getBundleShortName(MiscellaneousHelper::getBundleName($this)));

        $nodeBuilder = $treeBuilder->getRootNode()->children();

        foreach(static::AVAILABLE_NODES as $node) {
            $nodeBuilder = $nodeBuilder->{strtolower($node['type']) . 'Node'}($node['name'])->end();
        }

        return $treeBuilder;
    }
    
}