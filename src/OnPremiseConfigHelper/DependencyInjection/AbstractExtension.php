<?php

declare(strict_types=1);

namespace WoellUndWoell\PimcoreHelper\OnPremiseConfigHelper\DependencyInjection;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Yaml\Yaml;
use WoellUndWoell\PimcoreHelper\OnPremiseConfigHelper\Helper\MiscellaneousHelper;

abstract class AbstractExtension extends Extension
{
    
    /**
     * Merges the on-premise-config with the existing configs according to the Config object provided
     *
     * @param array                  $configArray
     * @param ConfigurationInterface $configuration
     *
     * @return array
     */
    protected function mergeConfigsWithOnPremiseConfigs(
        array $configArray,
        ConfigurationInterface $configuration
    ): array {

        $bundleNameSpace = MiscellaneousHelper::getBundleName($this);
        $bundleShortName = MiscellaneousHelper::getBundleShortName($bundleNameSpace);

        $onPremiseConfigFilePath = MiscellaneousHelper::getOnPremiseConfigFilePathForBundle($bundleNameSpace);

        if (\file_exists($onPremiseConfigFilePath)) {

            $onPremiseConfig = Yaml::parse(\file_get_contents($onPremiseConfigFilePath));

            if (\is_array($onPremiseConfig) && isset($onPremiseConfig[$bundleShortName])) {
                $configArray[] = $onPremiseConfig[$bundleShortName];
            }

        }

        $processor = new Processor();

        return $processor->processConfiguration($configuration, $configArray);

    }


    /**
     * Stores the configs in the DI container so they can be accessed more easily
     *
     * @param array            $configs
     * @param ContainerBuilder $container
     * @param string           $containerParamName
     *
     * @return void
     */
    protected function putConfigsIntoContainer(
        array $configs,
        ContainerBuilder $container,
        string $containerParamName
    ): void {

        if (\substr($containerParamName, 0, 1) === '%' && \substr($containerParamName, -1, 1) === '%') {
            $containerParamName = \substr($containerParamName, 1, -1);
        }

        $container->setParameter($containerParamName, $configs);
    }


    /**
     * Returns the configuration object that is used to check the resultig config
     * 
     * @return ConfigurationInterface
     */
    abstract protected function getConfigurationObject(): ConfigurationInterface;


    /**
     * Returns the name of the container param which is used to access the configs
     *
     * @return string
     */
    abstract protected function getContainerParamNameForConfigs(): string;


    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {

        $this->putConfigsIntoContainer(
            $this->mergeConfigsWithOnPremiseConfigs(
                $configs,
                $this->getConfigurationObject()
            ),
            $container,
            $this->getContainerParamNameForConfigs()
        );

    }
}
