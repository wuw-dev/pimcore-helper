<?php

declare(strict_types=1);

namespace WoellUndWoell\PimcoreHelper\OnPremiseConfigHelper\Controller;

use Pimcore\Bundle\AdminBundle\Controller\AdminController;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;
use WoellUndWoell\PimcoreHelper\OnPremiseConfigHelper\DependencyInjection\AbstractConfiguration;
use WoellUndWoell\PimcoreHelper\OnPremiseConfigHelper\Helper\MiscellaneousHelper;

abstract class AbstractConfigEditController extends AdminController
{

    protected string $showRoute;
    protected string $saveRoute;
    protected string $configFormTemplate;
    protected array  $configArray;

    /**
     * AbstractConfigEditController constructor.
     *
     * @param string $showRoute          Route name for displaying the edit form
     * @param string $saveRoute          Route name for saving the edit form
     * @param string $configFormTemplate Path to the template of the config form
     * @param array  $config             The actual config of a bundle
     */
    public function __construct(string $showRoute, string $saveRoute, string $configFormTemplate, array $config)
    {

        $this->showRoute    = $showRoute;
        $this->saveRoute    = $saveRoute;
        $this->configFormTemplate = $configFormTemplate;
        $this->configArray        = [];

        // Collect all editable configs
        foreach ($this->getConfigNodes() as $node) {

            if (isset($node['isEditable']) && $node['isEditable'] === true) {
                
                if (empty($node['description'])) {
                    $node['description'] = $node['name'];
                }
                
                $this->configArray[$node['name']] = $node;
                $this->configArray[$node['name']]['value'] = $config[$node['name']] ?? null;
            }

        }

    }


    /**
     * Return all available config nodes
     *
     * @return array
     */
    abstract protected function getConfigNodes(): array;


    /**
     * Write the local config file
     *
     * @return bool
     */
    protected function writeConfig(): bool {

        $bundleNamespace = MiscellaneousHelper::getBundleName($this);
        $bundleShortName = MiscellaneousHelper::getBundleShortName($bundleNamespace);
        $configFile = MiscellaneousHelper::getOnPremiseConfigFilePathForBundle($bundleNamespace);
	    $configDir = dirname($configFile);

	    if (!file_exists($configDir)) {
		    mkdir(
			    directory: $configDir,
			    recursive: true
		    );
	    }

        $yamlArray = [];

        foreach($this->configArray as $node) {
            $yamlArray[$bundleShortName][$node['name']] = $node['value'];
        }

        $result = file_put_contents($configFile, Yaml::dump($yamlArray));

        return $result !== false;

    }


    /**
     * Process the POST data of the form
     *
     * @param InputBag $request
     * @return bool
     */
    protected function processFormData(InputBag $request): bool {

        $configChanged = false;

        foreach($this->configArray as $node) {

            $postValue = $request->get($node['name']);

            if ($node['type'] === AbstractConfiguration::NODE_TYPE_BOOL) {
                $postValue = ($postValue !== null);
            }

            if ($postValue === null || $postValue === $node['value']) {
                continue;
            }

            $this->configArray[$node['name']]['value'] = $postValue;
            $configChanged = true;

        }

        return $configChanged;

    }

    /**
     * Show the edit form
     *
     * @return Response
     */
    public function showConfigForm(): Response {

        return $this->render(
            $this->configFormTemplate,
            [
                'config'      => $this->configArray,
                'actionRoute' => $this->saveRoute,
            ]
        );

    }

    /**
     * Process the edit form
     *
     * @param Request $request
     * @return Response
     */
    public function saveConfigForm(Request $request): Response {

        $params = [];

        if ($request->getMethod() === Request::METHOD_POST) {

            $configChanged = $this->processFormData($request->request);
            $params['changed'] = ($configChanged ? 'yes' : 'no');

            if ($configChanged) {
                $params['saved'] = ($this->writeConfig() ? 'yes' : 'no');
            }

        }

        return $this->redirectToRoute($this->showRoute, $params);

    }
}
