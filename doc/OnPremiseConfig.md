# On Premise Config Helper
## Why?
Sometimes you want certain values, e.g. credentials, to be accessible through the configs but
you don't want to include them in your repository. Also you might want to be able to change them 
without having to have access to the file system.

## How?
We provide you with a set of classes that you can extend that add an additional layer on top of the
Symfony configs Pimcore uses. They rely on  a config file that is located in Pimcore's private `var`
dir under `bundles/$BUNDLE_NAME/onPremiseConfig.yaml`.

As you can guess from the file extension it is a YAML file. This file is not intended to live inside
a git repo or be otherwise included in version control but is to exist only on the server your
application actually runs on.

## What do I have to do?
### Just use the config layer itself
Extend `DependencyInjection\AbstractExtension` and implement the two necessary methods to return the
configuration object and a string that is used to access the configs from the container.

### Also use the GUI
**Please note that the provided example GUI is very rudimentary. It works but it could definitely 
look much better and provide a better user experience. Feel free to contribute.**

* Have your bundle's configuration object extend the `DependencyInjection\AbstractConfiguration`, define 
  your config nodes as shown in the example of the abstract class and expose them (as also shown).
* Add a controller that extends `Controller\AbstractConfigEditController` and implement the abstract 
  method to return the Config Nodes.
* Add a template for the GUI. You may copy the one we provide as an example. **This template is only 
  needed once**, but you may set up separate ones for each bundle. It is your code after all.
* Set up routes for GET and POST of the GUI form (the routes should point to the method `showConfigForm`
  and `saveConfigForm` of **your** controller). 
* Pass these routes and the template as well as your bundle's config array to the controller's 
  constructor.